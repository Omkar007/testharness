﻿////////////////////////////////////////////////////////////////////////
// THService.cs - Sender and Receiver Endpoints processes             //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * This Module implements the Sender and Receiver at Cleint End. It uses 
 * blocking queue to send and retrieve messages and processes the request,
 * reply messages.
 * 
 *  
/*
 * Build Process:
 * ==============
 * Files Required:
 * THService.cs
 *   
 * public Interface:  
 *   ServiceHost CreateChannel(string url);
 *   void sendRequests(Message msg);
 *   Message getResults();
 *   void routeRequests(Message msg);
 *    
 * Compiler Command:
 *   csc /t:exe THService.cs
 *   
 * Maintainence History:
 * ==================
 * ver 1.0 : 20 Nov 2016
 *   - first release
 * 
 */
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Messages;
using FileStreamWriter;
using BlockQueue;
using System.Threading;
using Logger;

namespace TestHarness
{
    class TestHarnessReceiver : IcommService
    {
        static BlockingQueue<Message> thRcvblockQue = null;
        static BlockingQueue<Message> thTestReqQueue = null;
        static  ServiceHost service;
        Thread rcvdThrd = null;
        
        //constructor processing messages and enqueuing to other queue so that processing can bedone further
        public TestHarnessReceiver ()
        {
                //string hostUrl = "http://localhost:8088/THService";
                //service =CreateRecvChannel(hostUrl);
                if (thRcvblockQue == null)
                { thRcvblockQue = new BlockingQueue<Message>(); }
                if (thTestReqQueue == null)
                { thTestReqQueue = new BlockingQueue<Message>(); }

                rcvdThrd = new Thread(() => {
                    while (true)
                    {
                        Message msg = getResults();
                        if ((msg.type).Equals("testRequest"))           //checking test type
                        {
                            thTestReqQueue.enqueueRequest(msg);
                        }
                    }
                });
                rcvdThrd.IsBackground = true;
                rcvdThrd.Start();      
        }

        //used to close the communication channel
       ~ TestHarnessReceiver()
        {
                service.Close();
        }

        //  Create ServiceHost for Communication service
        public ServiceHost CreateRecvChannel(string address)
        {
            Console.WriteLine("# Requirement 10(2) -- Instantiating Listner at Test Harness Receiver End");
            WSHttpBinding binding = new WSHttpBinding();
            Uri baseAddress = new Uri(address);
            service = new ServiceHost(typeof(TestHarnessReceiver), baseAddress);
            service.AddServiceEndpoint(typeof(IcommService), binding, baseAddress);
            service.Open();
            return service;
        }

        //used to send the messages to the other server
        public void sendRequests(Message testReqeust)
        {
            Console.WriteLine("# Requirement 2(2),4(2) --- Message from client received at Test Harness Receiver Endpoint:\n    {0}", testReqeust.ToString());
            thRcvblockQue.enqueueRequest(testReqeust);
        }

        //used by receiver to dequeue the messages
        public Message getResults()
        {
            return thRcvblockQue.dequeueRequests();
        }

        //test stub
        static void Main(string[] args)
        {
            Console.Title = "Test Harness";
            Console.Write("\n ============= Test Harness ========================\n");
            Executive execManager = new Executive();
            string repositoryPath = "..//..//..//TestHarness//dllRepository";                //Path where directories of author Name will be created
            string hostUrl = "http://localhost:8088/THService";
          
            try
            {
                TestHarnessReceiver thRevcs = new TestHarnessReceiver();
                thRevcs.CreateRecvChannel(hostUrl);
                if (thTestReqQueue!=null) { execManager.processTests(repositoryPath, thTestReqQueue); }                
                Console.ReadKey();     
            }
            catch (Exception ex)
            {
                Console.Write("\n\n  {0}\n\n", ex.Message);
                return;
            }
        }
    }

    class TestHarnessSender
    {
        IcommService svcr;
        BlockingQueue<Message> sndBlockQRepository = null;
        Thread sndThrd = null;
        int tryCount = 0, MaxCount = 5;
        string lastError = "";

        //used to create channel and send the messages to the other server using commn channel
        void ThreadProc(string url)
        { try
            { while (true)
                {
                    Message msg = sndBlockQRepository.dequeueRequests();
                    if (url.Equals(msg.toUrl)) { }
                    else
                    {    while (true)
                        {  try
                            {    svcr = CreateProxy<IcommService>(msg.toUrl);
                                tryCount = 0;
                                break;
                            }
                            catch (Exception ex)
                            {
                                if (++tryCount < MaxCount)
                                    Thread.Sleep(100);
                                else
                                {
                                    lastError = ex.Message;
                                    break;
                                }
                            } } }
                    svcr.sendRequests(msg);
                    if (msg.body == "quit")
                        break;
                }}
            catch (Exception) { }
        }

        //constructor for communication channel creation using url and sending messages via thread proc method
     public TestHarnessSender(string url)
        {
            sndBlockQRepository = new BlockingQueue<Message>();
            while (true)
            {
                try
                {  svcr = CreateProxy<IcommService>(url);
                    tryCount = 0;
                    break;
                }
                catch (Exception ex)
                {
                    if (++tryCount < MaxCount)
                        Thread.Sleep(100);
                    else
                    {
                        lastError = ex.Message;
                        break;
                    } }
            }
            sndThrd = new Thread(() => ThreadProc(url));
            sndThrd.IsBackground = true;
            sndThrd.Start();
        }

        //used to create proxy channel for sending reqeuest to listener
        public C CreateProxy<C>(string url)
        {
            WSHttpBinding binding = new WSHttpBinding();
            EndpointAddress address = new EndpointAddress(url);
            ChannelFactory<C> factory = new ChannelFactory<C>(binding, address);
            return factory.CreateChannel();
        }

        //used to close th channel of communication
        public void Close()
        {
            ChannelFactory<IcommService> temp = (ChannelFactory<IcommService>)svcr;
            temp.Close();
        }

        //used by others to enqueue messages in sender queue of TH
        public void routeRequests(Message msg)
        {
            sndBlockQRepository.enqueueRequest(msg);
        }
    }
}
