﻿//////////////////////////////////////////////////////////////////////////
//// Listener.cs - Accepts users Test Requests an Enqueue these requests
//// Executive.cs - Dequeues Xml Requests, create child app domain
////                                                                    //
//// Platform:    HP Pavilion, Windows 10                               //
//// Application: CSE681 - Software Modelling Analysis, Test Harness    //
//// Author:      Omkar Patil, Syracuse University,                     //
////              ospatil@syr.edu, (315) 949-8810                       //
//////////////////////////////////////////////////////////////////////////
///*
// * Module Operations:
// * ==================
// * This module provides operations to get the test requests from the user 
// * and instantiate Blocking Queue
// * Object and Enqueue the requests.
// * This module provides operations to Create a child Application Domain,
// * load libraries into it, and run tests on all loaded libraries that
// * support the ITest interface. 
// * 
// * In order to load libraries without requiring the Tester to bind to
// * the types they declare, a Loader library is defined that is loaded
// * into the child domain, and loads each of the test libraries from
// * within the child. 
// * 
// * Test configurations are defined by the set of all libraries found
// * in a configuration directory.  Each configuration runs on its own
// * thread.  Test results are returned as a string. 
///*
// * Build Process:
// * ==============
// * Files Required:
// *   Listener.cs
// *   SemiExpQueue.cs
// *   Executive.cs
// *   Itest.cs
// *   Parser.cs
// *   LogDetails.cs
// *   
// * Compiler Command:
// *   csc /t:exe Listener.cs
// *   csc /t:exe SemiExpQueue.cs
// *   csc /t:exe Executive.cs
// *   csc /t:exe Itest.cs
// *   csc /t:exe Parser.cs
// *   csc /t:exe LogDetails.cs
// *   
// * Maintainence History:
// * ==================
// * ver 1.0 : 05 Oct 2016
// *   - first release
// * 
// */
////
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.IO;
//using BlockQueue;
//using System.Xml;
//using Logger;

//namespace TestHarness
//{
//    class Listener
//    {
//        [STAThread]
//        static void Main(string[] args)
//        {
//            string filePath = args[0];
//            string repositoryPath = args[1];

//            if (File.Exists(filePath))
//            {
//                Console.WriteLine("\n Tester, Demonstrates Test Harness v1.0");
//                Console.WriteLine("======================================================================");
//                SemiExpQueue queueService = new SemiExpQueue();             

//                Console.WriteLine("\n ============== Requirement 3 =================");
//                Console.WriteLine("Enqueuing Test Request...{0}", args[0]);
//                queueService.processRequest(filePath);
//                Executive execManager = new Executive();
//                execManager.processTests(repositoryPath,queueService);

//                Console.WriteLine("\n ============== Requirement 8 =================");
//                Console.WriteLine("Client Query for Test Request from Log Storage using Author Name");
//                Console.WriteLine("----------------------------------------------------");
//                LogDetails showClientLog = new LogDetails();
//                showClientLog.showAllLog(repositoryPath,args[2]);
//                Console.ReadKey();
//            }
//            else
//            {
//                Console.WriteLine("File does not exist. Please Enter Correct Path");
//            }

//        }
//    }
//}
