﻿////////////////////////////////////////////////////////////////////////
// Executive.cs - Dequeues Xml Requests, create child app domain and  //  
//                processes them.                                     //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * This module provides operations to get the test requests from the 
 * Receiver queue and processes it further by parsing the Message and 
 * deserializing the xml.
 * This module provides operations to Create a child Application Domain,
 * load libraries into it, and run tests on all loaded libraries that
 * support the ITest interface. 
 * 
 * In order to load libraries without requiring the Tester to bind to
 * the types they declare, a Loader library is defined that is loaded
 * into the child domain, and loads each of the test libraries from
 * within the child. 
 * 
 * Test configurations are defined by the set of all libraries found
 * in a configuration directory.  Each configuration runs on its own
 * thread.  Test results are returned as a string. 
/*
 * Build Process:
 * ==============
 * Files Required:
 *   
 *   SemiExpQueue.cs
 *   Executive.cs
 *   Itest.cs
 *   Message.cs
 *   LogDetails.cs
 *   Serialization.cs
 *   IcommService.cs
 *   IStreamService.cs
 *   
 * Compiler Command:
 * 
 *   csc /t:exe SemiExpQueue.cs
 *   csc /t:exe Executive.cs
 *   csc /t:exe Itest.cs
 *   csc /t:exe Message.cs
 *   csc /t:exe LogDetails.cs
 *   csc /t:exe Serialization.cs

 * Maintainence History:
 * ==================
 * ver 1.0 : 05 Oct 2016
 *   - first release
 * 
 */
//
using System;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
using ITest;
using System.Text;
using BlockQueue;
using Messages;
using Utilities;
using TestHarnessMessages;
using FileStreamWriter;
using System.ServiceModel;
using HRTimer;
using System.Threading;
using Parser;

namespace TestHarness
{
    class Executive
    {
        string toUrl = null;
        string fromUrl = "http://localhost:8088/THService";
        string errMsg = null;
        string requestType = null;
        StringBuilder storeResults = new StringBuilder();
        StringBuilder userLogs = new StringBuilder();
        string FileHostingUrl = @"http://localhost:8000/StreamService";
        static object lockObject = new object();

        //This method dequeues test request,deserializes xml,create folder and get dlls from repository,create child appdomain and execute tests by loading dlls
        public void processTests(string repositoryPath, BlockingQueue<Message> queueService)
        {  try
            {   while (true)
                    { Message clientRequest = queueService.dequeueRequests();                                        
                    string xmlBody = clientRequest.body;
                    TestRequest newRequest = xmlBody.FromXml<TestRequest>(); //deserializing the xml body (ie . parsing)
                    Console.WriteLine(" # Requirement 4(2)--------Test Request details after parsing an xml body---------");
                    string authFileName = newRequest.author; HiResTimer hrt = new HiResTimer();
                    string thDestPath = "..\\..\\..\\TestHarness\\dllRepository\\"+authFileName;
                    Console.WriteLine(newRequest.ToString());
                    List<TestElement> testResults = newRequest.tests;                
                    AppDomain main = AppDomain.CurrentDomain;  hrt.Start();
                    string dirpath = createDirectory(repositoryPath,authFileName);
                    bool filesTransferred = getFilesFromRepsository(thDestPath,testResults);                 
                    if (filesTransferred==false) { requestType = "FilesRequest"; toUrl = "http://localhost:8087/ClientService"; errMsg = "Files are not in Repository. Please resend the test driver and test code files";
                        sendNotifications(toUrl,fromUrl,requestType, authFileName,errMsg); }
                    if (testResults.Count > 0 )
                    {   
                        AppDomain ad = AppDomain.CreateDomain("ProcessAppDomain");   // Create Child AppDomain
                        Console.WriteLine("# Requirement 4(2) -- Child App Domain is created as {0} to process test request", ad.FriendlyName);            
                        Processor execRequests = (Processor)ad.CreateInstanceAndUnwrap(typeof(Processor).Assembly.FullName,typeof(Processor).FullName);  // unwrap creates proxy to ChildDomain

                        if (dirpath!= null)
                        {    foreach (TestElement item in testResults) {  //Enumerate each test in list
                                Console.WriteLine("\n");
                                string tsPath = item.testDriver;
                                string testResult = null;
                                string loadResults = execRequests.LoadAssembly(dirpath, tsPath);
                                if (loadResults != null)
                                {   testResult = execRequests.execute(); }
                                string[] logs = testResult.Split('^');
                                userLogs.Append(logs[1]);
                                hrt.Stop();
                                Console.WriteLine("# Requirement 12(1) -- Time required for executing tests: {0}",hrt.ElapsedMicroseconds);
                                lock (lockObject)
                                { storeResults.Append(loadResults + Environment.NewLine + logs[0]); }                       }
                            requestType = "testResultsReply"; toUrl = "http://localhost:8087/ClientService";
                            sendTestResults(toUrl, fromUrl, requestType, authFileName, storeResults, userLogs);
                            requestType = "testResultsQuery"; toUrl = "http://localhost:8085/RepoService";
                            sendTestResults(toUrl, fromUrl, requestType, authFileName, storeResults, userLogs);
                        }
                        AppDomain.Unload(ad); Console.WriteLine("# Requirement 7(2) -- Unloading Child App Domain");}    // unloading ChildDomain, and so unloading the library           
                } }
            catch (Exception e)
            { Console.WriteLine(e.Message); }}

        //Used to get the files from repository server
        public bool getFilesFromRepsository(string thSavePath,List<TestElement> dllFiles)
        {
            try
            {
                bool flag = false;
                Thread t = new Thread(url =>
                {
                    StreamService srvce;
                    srvce = StreamService.NewHost((string)url);
                });
                t.Start(FileHostingUrl);
                Client clnt = new Client(FileHostingUrl);                
                HRTimer.HiResTimer hrt = new HRTimer.HiResTimer();
                List<string> dllsDownload = new List<string>();
                foreach (TestElement item in dllFiles)
                {                   
                    dllsDownload.Add(item.testDriver);
                    foreach (string tcCodes in item.testCodes)
                    { dllsDownload.Add(tcCodes); }
                }
                Console.WriteLine("# Requirement 6(2) -- Files Downloaded to the Test Harness Server from Repository Server");
                hrt.Start();                   
                foreach (string dlls in dllsDownload)
                {
                    flag = clnt.CheckFileAvailability(dlls);
                    clnt.download(thSavePath, dlls);
                }
                hrt.Stop();
                ulong time = hrt.ElapsedMicroseconds;        //timer to calculate time elapsed
                Console.WriteLine("# Requirement 12(1) -- Throughput for entire process of File download \n {0}", time + "ms");
                return flag;
            }
            catch (Exception)
            { return false; }
            }

        //create a directory from author name where dll files will be downloaded
        public string createDirectory(string repositoryPath,string authorName)
        {
            string dirPath = repositoryPath+"/"+authorName;
            try
            {
                if (Directory.Exists(dirPath)){}
                else
                {               // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(dirPath);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\nDirectory cannot be created due to exception {0}",e.Message);
            }    
            return dirPath;
        }

        //This method is used to send the messages to the client or repository in case of absence of dlls in repository
        public void sendNotifications(string toUrl, string fromUrl, string type, string author, string body)
        {
            try
            {
                HRTimer.HiResTimer hrt = new HRTimer.HiResTimer();
                hrt.Start();
                TestHarnessSender client = initializeSender(toUrl);
                TestMessages msg = new TestMessages();
                Message thresponse = msg.makeMessage(toUrl, fromUrl, type, author, body);
                client.routeRequests(thresponse);
                hrt.Stop();           
                Console.WriteLine("\nTime Elapsed in sending dll files resend request message {0} ms",hrt.ElapsedMicroseconds);
            }
            catch (Exception e)
            {
                Console.WriteLine("\nException occurred in sending file resend Request Message to Client {0}",e.Message);
            }
        }

        //used to initialize testharness sender by url as argument
        public TestHarnessSender initializeSender(string url)
        {
            try
            {
                TestHarnessSender client = new TestHarnessSender(url);
                return client;
            }
            catch (Exception e)
            {
                Console.WriteLine("\nException occurred in Initializing Sender {0}",e.Message);
                return null;
            }
        }

        //used to send the test results to repository and client server
        public void sendTestResults(string toUrl, string fromUrl, string type, string author,StringBuilder testResults, StringBuilder queryLogs)
        {
            try
            {
                HRTimer.HiResTimer hrt = new HRTimer.HiResTimer();
                hrt.Start();
                TestHarnessSender client = initializeSender(toUrl);
                TestMessages msg = new TestMessages();
                string body = "-----Test Results---- \n"+ testResults.ToString() +"\n----Query Logs----\n"+ queryLogs.ToString();
                Message thresponse = msg.makeMessage(toUrl, fromUrl, type, author,body);             
                client.routeRequests(thresponse);
                hrt.Stop();
                if (type.Equals("testResultsReply"))
                {
                    Console.WriteLine("\nTime Elapsed in sending test results and query logs message to Client {0} ms", hrt.ElapsedMicroseconds);
                }
                else if (type.Equals("testResultsQuery"))
                {   
                    Console.WriteLine("\nTime Elapsed in sending test and query logs storage request to Repository {0} ms", hrt.ElapsedMicroseconds);
                }         
            }
            catch (Exception e)
            {
                Console.WriteLine("\nExeption occurred in sending TestResults {0}",e.Message);
            }
        }
    }


    class Processor : MarshalByRefObject
    {
        private List<TestData> testDriver = new List<TestData>();
        public override object InitializeLifetimeService()
        {
            return null;
        }

        //This function loads the dll files into the child appdomain
        public string LoadAssembly(string path, string tsPath)
        {   string loadResult = null;
            string dllFile = null;
            try
            {   string[] files = System.IO.Directory.GetFiles(path, "*.dll", SearchOption.AllDirectories);
                foreach (string file in files)
                { int index = file.LastIndexOf("\\");
                    dllFile = file.Substring(index + 1);                
                    if (dllFile.Equals(tsPath))
                    {
                        Assembly assem = Assembly.LoadFrom(file); //Loading assemblies into child AppDomain
                        Type[] types = assem.GetExportedTypes();
                        loadResult = "# Requirement 4(2) -- Loading dll File " + dllFile + " in " + AppDomain.CurrentDomain.FriendlyName;
                        foreach (Type t in types)
                        {  if (t.IsClass && typeof(Itest).IsAssignableFrom(t))  // does this type derive from ITest ?
                            {  Itest tdr = (Itest)Activator.CreateInstance(t);    // create instance of test driver
                                                                                  // save type name and reference to created type on managed heap
                                TestData td = new TestData();
                                td.Name = t.Name;
                                td.testDriver = tdr;
                                testDriver.Add(td);
                                Console.WriteLine("Test Driver derived from Itest Interface is {0} at line no 276 ",td.testDriver+" Req 5(1)");
                            }  }
                    } }
            }
            catch (Exception e)
            {   loadResult = "Loading from repository.." + dllFile + Environment.NewLine + e.Message;
            }
            return loadResult;
        }

        // Executes tests in libraries that support Itest Interface.
        public string execute()
        {
            string testResult = null;
            string userLog = null;
            try
            {
                if (testDriver.Count == 0)
                    return null;
                for (int i = 0; i < testDriver.Count; i++)  // enumerate the test list
                {
                    testResult = "# Requirement 4(2) -- Testing ..." + testDriver[i].testDriver + " in ...." + AppDomain.CurrentDomain.FriendlyName;
                    if (testDriver[i].testDriver.test() == true)
                    {
                        testResult += Environment.NewLine + "Test passed \n";
                    }
                    else
                    {
                        testResult += Environment.NewLine + "Test failed \n";
                    }
                    userLog = testDriver[i].testDriver.getLog() + Environment.NewLine;
                    testDriver.Remove(testDriver[i]);
                }
            }
            catch (Exception e)
            {
                testResult += Environment.NewLine + e.Message;
            }
            return testResult + "^" + userLog;
        }
    }
}
