﻿////////////////////////////////////////////////////////////////////////
// TestDriver.cs - Test Driver to test division of two Numbers       //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * This module implements the bool test function derived from Itest Interface
 * and test the division of two numbers
 *  
/*
 * Build Process:
 * ==============
 * Files Required:
 *   TestDriver.cs
 *   
 * Compiler Command:
 *   csc /t:exe TestDriver.cs
 *   
 * Maintainence History:
 * ==================
 * ver 1.0 : 05 Oct 2016
 *   - first release
 * 
 */
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITest;
using TestCode1;

namespace TestDriver1
{

    public class TestDriver : Itest
    {
        private CodeToTest1 code;

        // initializing codeToTest2 object
        public TestDriver()
        {
            code = new CodeToTest1();
        }

        //initializes testdriver 
        public static Itest create()
        {
            return new TestDriver();
        }

        // This function calls test case, runs it and returns result
        public bool test()
        {
            int a = 10;
            int b = 2;
            int k = code.divide(a, b);
            if (k==5)
            {
                return true;
            }
            else
            {
                return false;
            }
         
        }

        //get log function implementation
        public string getLog()
        {
            return "Result of Division matches to the Expected one";
        }

        //test stub
        public static void Main(string[] args)
        {
            Itest test = TestDriver.create();

            if (test.test() == true)
                Console.Write("\n  test passed");
            else
                Console.Write("\n  test failed");
            Console.Write("\n\n");

        }
    }
}
