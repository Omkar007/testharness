﻿using FileStreamWriter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestHarnessMessages;

namespace Utilities
{
    public class SendDllFiles
    {
        string FileHostingUrl = @"http://localhost:8000/StreamService";

        //used to send the dll files to repsository in form of streams
        public bool sendDllFiles(string toSendPath,List<string> dllFiles)
        {
            try
            {              
                Thread t = new Thread(url =>
                {    StreamService srvce;
                    srvce = StreamService.NewHost((string)url);
                });
                t.Start(FileHostingUrl);
                Client clnt = new FileStreamWriter.Client(FileHostingUrl);
                HRTimer.HiResTimer hrt = new HRTimer.HiResTimer();    
                hrt.Start();
                foreach (string dlls in dllFiles)
                {
                    clnt.uploadFile(toSendPath, dlls);              
                }
                t.Join();
                hrt.Stop();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


    }
}
