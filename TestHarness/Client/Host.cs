﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using ICommunicator;


namespace Client
{
    class Host
    {
        static ServiceHost CreateChannel(string url)
        {
            WSHttpBinding binding = new WSHttpBinding();
            Uri address = new Uri(url);
            Type service = typeof(Service);
            ServiceHost host = new ServiceHost(service, address);
            host.AddServiceEndpoint(typeof(IcommService), binding, address);
            return host;
        }
        //static void Main(string[] args)
        //{
        //    Console.Title = "WSHttp CLient side Service Host";
        //    Console.Write("\n =====================================\n");

        //    ServiceHost host = null;
        //    try
        //    {
        //        host = CreateChannel("http://localhost:8080/Service");
        //        host.Open();
        //        Console.Write("\n  Started WSHttpService - Press key to exit:\n");
        //        Console.ReadLine();
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.Write("\n\n  {0}\n\n", ex.Message);
        //        return;
        //    }
        //    host.Close();
        //}
    }
}
