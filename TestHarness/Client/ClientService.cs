﻿////////////////////////////////////////////////////////////////////////
// ClientService.cs - Sender and Receiver Endpoints processes         //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * This Module implements the Sender and Receiver at Client End. It uses 
 * blocking queue to send and retrieve messages and processes the request,
 * reply messages.
 * 
 *  
/*
 * Build Process:
 * ==============
 * Files Required:
 * ClientService.cs
 *   
 * public Interface:  
 *   ServiceHost CreateChannel(string url);
 *   void dispTestResultsMsg(BlockingQueue<Message> testResultsQ);
 *   void sendRequests(Message msg);
 *   Message getResults();
 *   void dispFilesMsg(BlockingQueue<Message> filesQ);
 *   void routeRequests(Message msg);
 *    
 * Compiler Command:
 *   csc /t:exe ClientService.cs
 *   
 * Maintainence History:
 * ==================
 * ver 1.0 : 20 Nov 2016
 *   - first release
 * 
 */
//
using System.Threading;
using System.ServiceModel;
using Messages;
using BlockQueue;
using FileStreamWriter;
using System;
using Utilities;
using TestHarnessMessages;
using System.Collections.Generic;

namespace ClientService
{
    public class Receiver : IcommService
    {
        static BlockingQueue<Message> rcvBlockingQ = null;
        static ServiceHost service;

        public Receiver ()
        {
                if (rcvBlockingQ == null)
                { rcvBlockingQ = new BlockingQueue<Message>();
                }
        }

        //used to close the service channel
        ~Receiver()
        {
                if (service!=null)
                {   service.Close(); }             
        }

        //used to close the service channel
        public void Close()
        {
                if (service != null)
                { service.Close(); }
        }

        //  Create ServiceHost for Communication service
        public ServiceHost CreateRecvChannel(string address)
        {
            WSHttpBinding binding = new WSHttpBinding();
            Uri baseAddress = new Uri(address);
            service = new ServiceHost(typeof(Receiver), baseAddress);
            service.AddServiceEndpoint(typeof(IcommService), binding, baseAddress);
            service.Open();
            return service;
        }

        //used to send the message
        public void sendRequests(Message msg)
        {
            rcvBlockingQ.enqueueRequest(msg);
        }

        //used to dequeue the messages enqueued in the queue
        public Message getResults()
        {           
            return rcvBlockingQ.dequeueRequests();
        }

        // Test method for main to generate test request
        public static Message testMethod()
        {
            TestElement te1 = new TestElement();
            te1.testName = "Test1";
            te1.addDriver("TestDriver1.dll");
            te1.addCode("TestCode1.dll");

            TestElement te2 = new TestElement();
            te2.testName = "Test2";
            te2.addDriver("TestDriver2.dll");
            te2.addCode("TestCode2.dll");

            TestRequest tr = new TestRequest();
            tr.author = "Omkar Patil";
            tr.tests.Add(te1);
            tr.tests.Add(te2);
            string trXml = tr.ToXml();

            Message msg = new Message();
            msg.toUrl = "http://localhost:8088/THService";
            msg.fromUrl = "http://localhost:8087/ClientService"; 
            msg.type = "testRequest";
            msg.author = "Omkar Patil";
            msg.body = trXml;
       
            return msg;
        }

        static void Main(string[] args)
        {  try
            {   Console.Title = "Client";
                Console.Write("\n ================= Client ===========================\n");
                Receiver clntRcvr = new Receiver();
                clntRcvr.CreateRecvChannel("http://localhost:8087/ClientService");
                string url = "http://localhost:8088/THService";
                Sender client = new Sender(url);
                // Test Request Message sent from Client to Test Harness Server
                Message msg = testMethod();
                Console.WriteLine("test Requests Message to be sent is {0} \n", msg);
                client.routeRequests(msg);
                SendDllFiles sendFiles = new SendDllFiles(); List<string> dllFiles = new List<string>();
                dllFiles.Add("TestDriver1.dll"); dllFiles.Add("TestDriver2.dll");
                dllFiles.Add("TestCode1.dll"); dllFiles.Add("TestCode2.dll");
                sendFiles.sendDllFiles("..\\..\\..\\TestHarness\\dllRepository", dllFiles);

                // Test and Query log Request Message sent from Client to Repository Server
                string urlLogReq = "http://localhost:8085/RepoService";
                Sender client1 = new Sender(urlLogReq);
                Message testLogMsg = new Message(); testLogMsg.toUrl = urlLogReq;  testLogMsg.fromUrl = "http://localhost:8087/ClientService";
                testLogMsg.type = "LogsQuery";testLogMsg.author = "Omkar Patil";testLogMsg.body = "ShowAllLogFiles";
                client1.routeRequests(testLogMsg);

                Message queryFileLog = new Message();
                queryFileLog.toUrl = urlLogReq; queryFileLog.fromUrl = "http://localhost:8087/ClientService";
                queryFileLog.type = "LogsQuery";queryFileLog.author = "Omkar Patil"; queryFileLog.body = "Omkar Patil_2016_11_19-20_01_34.txt";
                client1.routeRequests(queryFileLog);
                Console.ReadKey();
            }
            catch (Exception ex)
            {   Console.Write("\n\n  {0}\n\n", ex.Message);       return;
            }}
    }

    public class Sender
    {
        IcommService svcr;
        BlockingQueue<Message> sndBlockingQ = null;
        Thread sndThrd = null;
        int tryCount = 0, MaxCount = 5;
        string lastError = "";

        //used to create the connection and send the message to the other server
        void ThreadProc(string url)
        {
            try
            {while (true)
                {
                    Message msg = sndBlockingQ.dequeueRequests();
                    if (url.Equals(msg.toUrl)) { }
                    else
                    { while (true)
                        { try
                            { svcr = CreateProxy<IcommService>(msg.toUrl);
                                tryCount = 0;
                                break;
                            }
                            catch (Exception ex)
                            {
                                if (++tryCount < MaxCount)
                                    Thread.Sleep(100);
                                else
                                {
                                    lastError = ex.Message;
                                    break;
                                }} }
                    }
                    svcr.sendRequests(msg);
                    if (msg.body == "quit")
                        break;
                } }
            catch (Exception e)
            { Console.WriteLine("Exeption occured in client service sender thread {0}",e.Message);}
        }

        //constructor creating channel and calling the thread proc to send messages
      public Sender(string url)
        {
            sndBlockingQ = new BlockingQueue<Message>();
            while (true)
            {  try
                {
                    svcr = CreateProxy<IcommService>(url);
                    tryCount = 0;
                    break;
                }
                catch (Exception ex)
                {
                    if (++tryCount < MaxCount)
                        Thread.Sleep(100);
                    else
                    {
                        lastError = ex.Message;
                        break;
                    }
                }
            }
            sndThrd = new Thread(() => ThreadProc(url));
            sndThrd.IsBackground = true;
            sndThrd.Start(); 
        }

        //Used to create a proxy channel for sending messages to the Server
        public C CreateProxy<C>(string url)
        {
            WSHttpBinding binding = new WSHttpBinding();
            EndpointAddress address = new EndpointAddress(url);
            ChannelFactory<C> factory = new ChannelFactory<C>(binding, address);
            return factory.CreateChannel();
        }

        //used to close the channel after sending reqeusts
        public void Close()
        {
            ChannelFactory<IcommService> temp = (ChannelFactory<IcommService>)svcr;
            temp.Close();
        }

        //used to enqueue reqeuests posted at this senders queue
        public void routeRequests(Message msg)
        {
            sndBlockingQ.enqueueRequest(msg);
        }

    }
}
