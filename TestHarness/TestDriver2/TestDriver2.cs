﻿////////////////////////////////////////////////////////////////////////
// TestDriver.cs - Test Driver to test Addition of two Numbers        //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * This module implements the bool test function derived from Itest Interface
 * and test the addition of two numbers
 *  
/*
 * Build Process:
 * ==============
 * Files Required:
 *   TestDriver2.cs
 *   
 * Compiler Command:
 *   csc /t:exe TestDriver2.cs
 *   
 * Maintainence History:
 * ==================
 * ver 1.0 : 05 Oct 2016
 *   - first release
 * 
 */
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITest;
using TestCode2;

namespace TestDriver2
{
    public class TestDriver2: Itest
    {
        private CodeToTest2 code;

        // initializing codeToTest2 object
        public TestDriver2()
        {
            code = new CodeToTest2();
        }

        public static Itest create()
        {
            return new TestDriver2();
        }


        // This function calls test case, runs it and returns result
        public bool test()
        {
            int a = 6;
            int b = 2;
            int k = code.addition(a, b);
            if (k== 8)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //get log function demonstrates users log
        public string getLog()
        {
            return "Addition of Two numbers 6 and 2 is Incorrect";
        }

        //test stub
        public static void Main(string[] args)
        {
            Itest test = TestDriver2.create();

            if (test.test() == true)
                Console.Write("\n  test passed");
            else
                Console.Write("\n  test failed");
            Console.Write("\n\n");

        }

    }
}
