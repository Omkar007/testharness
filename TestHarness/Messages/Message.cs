﻿////////////////////////////////////////////////////////////////////////
// Messages.cs - Defines the attribute so that this structure can be  //
// used to send messages among client,Test Harness,Repository         //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * Defines the attribute so that this structure can be
 * used to send messages among client,Test Harness,Repository.
 *  
/*
 * Build Process:
 * ==============
 * Files Required:
 *   Messages.cs
 *   
 * public Interface:  
 *   Message makeMessage(string toUrl, string fromUrl, string type, string author, string body);
 *   
 *   
 * Compiler Command:
 *   csc /t:exe Messages.cs
 *   
 * Maintainence History:
 * ==================
 * ver 1.0 : 20 Nov 2016
 *   - first release
 * 
 */
//

using System;
using System.Linq;
using System.Xml.Linq;

namespace Messages
{

    [Serializable]
    public class Message
    {
        public string toUrl { get; set; }
        public string fromUrl { get; set; }
        public string type { get; set; }
        public string author { get; set; } = "";
        public DateTime time { get; set; } = DateTime.Now;
        public string body { get; set; } = "";

        public Message(string bodyStr = "")
        {
            body = bodyStr;
            type = "undefined";
        }

        //used to parse string in message
        public Message fromString(string msgStr)
        {
            Message msg = new Message();
            try
            {
                string[] parts = msgStr.Split(',');
                for (int i = 0; i < parts.Count(); ++i)
                    parts[i] = parts[i].Trim();

                msg.toUrl = parts[0].Substring(4);
                msg.fromUrl = parts[1].Substring(6);
                msg.type = parts[2].Substring(6);
                msg.author = parts[3].Substring(8);
                msg.time = DateTime.Parse(parts[4].Substring(6));
                if (parts[5].Count() > 6)
                    msg.body = parts[5].Substring(6);
            }
            catch
            {
                Console.Write("\n  string parsing failed in Message.fromString(string)");
                return null;
            }
            return msg;
        }

        //used to override the tostring method
        public override string ToString()
        {
            string temp = "to: " + toUrl;
            temp += ", from: " + fromUrl;
            temp += ", type: " + type;
            if (author != "")
                temp += ", author: " + author;
            temp += ", time: " + time;
            temp += ", body:\n" + body;
            return temp;
        }

        //used to copy the message 
        public Message copy(Message msg)
        {
            Message temp = new Message();
            temp.toUrl = msg.toUrl;
            temp.fromUrl = msg.fromUrl;
            temp.type = msg.type;
            temp.author = msg.author;
            temp.time = DateTime.Now;
           // temp.testReqName = msg.testReqName;
            temp.body = msg.body;
            return temp;
        } 
    }

    public static class extMethods
    {
        //used to show the formatted message
        public static void show(this Message msg, int shift = 2)
        {
            Console.Write("\n  formatted message:");
            string[] lines = msg.ToString().Split(',');
            foreach (string line in lines)
                Console.Write("\n    {0}", line.Trim());
            Console.WriteLine();
        }

        //used to shift lines to display file string in appropriate manner
        public static string shift(this string str, int n = 2)
        {
            string insertString = new string(' ', n);
            string[] lines = str.Split('\n');
            for (int i = 0; i < lines.Count(); ++i)
            {
                lines[i] = insertString + lines[i];
            }
            string temp = "";
            foreach (string line in lines)
                temp += line + "\n";
            return temp;
        }

        //used to format the xml
        public static string formatXml(this string xml, int n = 2)
        {
            XDocument doc = XDocument.Parse(xml);
            return doc.ToString().shift(n);
        }
    }

    public class TestMessages
    {
        //used to create message on basis of serveral arguments 
        public Message makeMessage(string toUrl, string fromUrl, string type, string author, string body)
        {
            Message msg = new Message();
            msg.toUrl = toUrl;
            msg.fromUrl = fromUrl;
            msg.type = type;
            msg.author = author;
            msg.time = DateTime.Now;
            msg.body = body;
            return msg;
        }

        //test stub
        static void Main(string[] args)
        {
            Console.Write("\n  Testing Message Class");
            Console.Write("\n =======================\n");

            Message msg = new Message();
            msg.toUrl = "TH";
            msg.fromUrl = "CL";
            msg.type = "basic";
            msg.author = "Omkar";
            msg.body = "    a body";
         //   msg.testReqName = "TestRequest1";
            Console.Write("\n  base message:\n    {0}", msg.ToString());
            Console.WriteLine();
            msg.show();
            Console.WriteLine();
            Console.Write("\n  Testing Message.fromString(string)");
            Console.Write("\n ------------------------------------");
            Message parsed = msg.fromString(msg.ToString());
            parsed.show();
            Console.WriteLine();
            Console.ReadKey();
        }
    }
    }
