﻿////////////////////////////////////////////////////////////////////////
// SemiExpQueue.cs - Enqueue and Dequeue Test Requests                //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * This module provides the GUI functionalities for user and dispatches the 
 * request to worker thread for processing and returns back the result on 
 * the GUI.
 *  
/*
 * Build Process:
 * ==============
 * Files Required:
 *   MainWindow.xaml.cs
 *   
 * public Interface:  
 *    void ThreadProc();
 *    void dispFilesMsg(BlockingQueue<Message> filesQ);
 *    void dispTestResultsMsg(BlockingQueue<Message> testResultsQ);
 *    Message generateTestRequest();
 *    List<string> dllstobeSent();
 *    Sender intialize(string toUrl);
 *    void SendFiles_Click(object sender, RoutedEventArgs e);
 *    void GetFileLog_Submit_Click(object sender, RoutedEventArgs e);
 *    void SubmitAllRequests_Click(object sender, RoutedEventArgs e);
 *    void Query_Submit_Click(object sender, RoutedEventArgs e);
 *    void Window_Unloaded(object sender, RoutedEventArgs e);
 *    void OnNewMessageHandler(string msg);
 *      
 * Compiler Command:
 *   csc /t:exe MainWindow.xaml.cs
 *   
 * Maintainence History:
 * ==================
 * ver 1.0 : 20 Nov 2016
 *   - first release
 * 
 */
//
using System;
using System.Windows;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;
using System.Threading;
using System.Text;
using System.ServiceModel;
using ClientService;
using Messages;
using TestHarnessMessages;
using Utilities;
using System.Collections.Generic;
using FileStreamWriter;
using BlockQueue;
using HRTimer;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Sender sndr;
        Receiver recvr;
        Message recvdMsg;
        Thread rcvThrd = null;
        static bool isDefined=false;
         BlockingQueue<Message> tstResultsBlockQ = null;
         BlockingQueue<Message> filesBlockQue = null;  

        //Method used to dequeue request and enqueue it in other queue to avoid the delay in processing of requests
        void ThreadProc()
        {
            if (tstResultsBlockQ == null)
            {  tstResultsBlockQ = new BlockingQueue<Message>();
            }
            if (filesBlockQue == null)
            {filesBlockQue = new BlockingQueue<Message>();
            }
            rcvThrd = new Thread(() =>
            {  while (true)
                {   // get message out of receive queue - will block if queue is empty
                    recvdMsg = recvr.getResults();
                    if (recvdMsg != null)
                    {
                        if ((recvdMsg.type).Equals("FilesRequest"))
                        {  filesBlockQue.enqueueRequest(recvdMsg);
                        }
                        else if ((recvdMsg.type).Equals("testResultsReply") || (recvdMsg.type).Equals("LogsReply") || (recvdMsg.type).Equals("storeLogsReply") || (recvdMsg.type).Equals("LogFileReply"))
                        {      tstResultsBlockQ.enqueueRequest(recvdMsg);
                        }
                        else
                            Console.WriteLine("Enqueuing Message...Failed");}
                }
            });     
            rcvThrd.IsBackground = true;
            rcvThrd.Start();
        }

        //used to display the file Messages after dequeuing it on the GUI
        public void dispFilesMsg(BlockingQueue<Message> filesQ)
        {
            Thread dllfileThread = null;
            try
            {   dllfileThread = new Thread(() =>
                {
                    while (true)
                    {
                        Message filesReply = filesQ.dequeueRequests();
                        if (filesReply.type.Equals("FilesRequest"))
                        {  Console.WriteLine("# Requirement 3(1) -- files resend request message from Test Harness \n {0}", filesReply.body);
                            Dispatcher.Invoke(() => { OnNewMessageHandler("# Requirement 3(1) -- files resend request message from Test Harness \n" + filesReply.body); });
                        }
                    }
                });
                dllfileThread.IsBackground = true;
                dllfileThread.Start();
            }
            catch (Exception e)
            { Console.WriteLine("Exception occured while retrieving file related Messages from Queue {0}", e.Message); }
        }

        //used to display the test results after dequeuing it on the GUI
        void dispTestResultsMsg(BlockingQueue<Message> testResultsQ)
        {
            string msg = null;
            string res = null;
            Thread tstResultThread = null;
            try
            { tstResultThread = new Thread(() =>  {
                    while (true)
                    {
                        Message testResults = testResultsQ.dequeueRequests();
                        if (testResults.type.Equals("storeLogsReply"))
                        {  Console.WriteLine("# Requirement 7(2) -- Test Results and Query Logs stored acknowledgement: \n {0}", testResults.body);
                            msg = "# Requirement 7(2) -- Test Results and Query Logs stored acknowledgement: \n";
                            res = msg + testResults.body;
                        }
                        else if (testResults.type.Equals("testResultsReply"))
                        {   Console.WriteLine("# Requirement 7(2) -- Test Request Execution Detailed Log from Test Harness Server: \n {0}", testResults.body);
                             msg = "# Requirement 7(2) -- Test Request Execution Detailed Log from Test Harness Server: \n";
                             res = msg + testResults.body;
                       }
                        else if (testResults.type.Equals("LogsReply"))
                        { Console.WriteLine("# Requirement 9(1) -- Displaying all Logs of Author {0} from Repository Server \n {1}", testResults.author,testResults.body);
                            msg= "# Requirement 9(1) -- Displaying all Logs of Author from Repository Server \n";
                            res = msg + testResults.body;
                        }
                        else if (testResults.type.Equals("LogFileReply"))
                        { Console.WriteLine("# Requirement 9(1) -- Logs of requested file: \n {0}", testResults.body);
                            msg = "# Requirement 9(1) -- Logs of requested file: \n";
                            res = msg + testResults.body;
                         }
                    Dispatcher.Invoke(() => { OnNewMessageHandler(res); });
                    res = null;
                }
                });
                tstResultThread.IsBackground = true;
                tstResultThread.Start();
            }
            catch (Exception e)
            { Console.WriteLine("Exception occured while displaying test Results Msgs from Queue {0}", e.Message); }
        }

        //generate test request message from the information provided by user
        public  Message generateTestRequest()
        {
            TestElement te1 = new TestElement();
            te1.testName = "Test1";
            te1.addDriver(TestDriverBox1.Text);
            te1.addCode(TestCode1.Text);

            TestElement te2 = new TestElement();
            te2.testName = "Test2";
            te2.addDriver(TestDriverBox2.Text);
            te2.addCode(TestCode2.Text);

            TestRequest tr = new TestRequest();
            tr.author = AuthorName.Text;
            if ((TestDriverBox1.Text!=null && TestDriverBox1.Text!="") && (TestCode1.Text!=null && TestCode1.Text!="")) {  tr.tests.Add(te1);}
            if ((TestDriverBox2.Text != null && TestDriverBox2.Text != "") && (TestCode2.Text != null && TestCode2.Text != "")) { tr.tests.Add(te2); }          
            string trXml = tr.ToXml();
            Message msg = new Message();
            msg.toUrl = RemoteAddressTextBox1.Text; 
            msg.fromUrl = LocalAddr1.Text;
            msg.type = "testRequest";
            msg.author = AuthorName.Text;
            msg.body = trXml;
            return msg;
        }

        //used to get list of all files provided by user
        public List<string> dllstobeSent()
        {
            List<string> dllFiles = new List<string>();
            dllFiles.Add(TestDriverBox1.Text);
            dllFiles.Add(TestCode1.Text);
            dllFiles.Add(TestDriverBox2.Text);
            dllFiles.Add(TestCode2.Text);

            return dllFiles;
        }

        //This function is used to initialize Sender with the url provided as an argument
        public Sender intialize(string toUrl)
        {
            string url = toUrl;
            sndr = new Sender(url);
            return sndr;
        }

        //this method sends fileto the repository server
        void sendFiles()
        {
            try
            {
                Console.WriteLine("# Requirement 2(2),6(2) -- Sending Files to the repository server");
                StreamService sc = new StreamService();
                string path = System.IO.Path.GetFullPath(dllPath.Text);
                SendDllFiles sndFiles = new SendDllFiles();
                bool flag = sndFiles.sendDllFiles(path, dllstobeSent());
                if (flag)
                {
                    Console.WriteLine("# Requirement 2(2),6(2)  -- Files Successfully sent to the repository server");
                    Dispatcher.Invoke(() => { OnNewMessageHandler("# Requirement 2(2),6(2)  -- Files Successfully sent to the repository server"); });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occured in sending files {0}", ex.Message);
            }
        }

        //This method triggers on send dll files and calls the file upload method to send files to repsoitory
        private void SendFiles_Click(object sender, RoutedEventArgs e)
        {
            sendFiles();
        }

        //It is used to get the logs on basis of file name provided by user
        private void GetFileLog_Submit_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                TestMessages createMsg = new TestMessages();
                Message queryReq = createMsg.makeMessage(RemoteLogRequestBox.Text, LocalLogRequestBox.Text, "LogsQuery", AuthorNameR.Text,FileNameForQuery.Text);
                Dispatcher.Invoke(() => { OnNewMessageHandler("# Requirement 9(1) -- Query File Request Message sent to the Repository: \n " + queryReq.ToString()); });
                sndr = intialize(RemoteLogRequestBox.Text);
                sndr.routeRequests(queryReq);
                ThreadProc();
                dispTestResultsMsg(tstResultsBlockQ);
            }
            catch (Exception ex)
            {      Console.WriteLine("Exception caused in querying single file log request in Client GUI {0}", ex.Message);
            }
        }

        //This method is used to demonstrate all the requirements for project 4 sends test request,query request and display logs
        private void SubmitAllRequests_Click(object sender, RoutedEventArgs e)
        { try
            {
                sendFiles();
                string msgToDisplay = null;
                HiResTimer hrt = new HiResTimer();
                hrt.Start();
     
                sndr = intialize(RemoteAddressTextBox1.Text);
                Message msg = generateTestRequest();
                msgToDisplay ="# Requirement 2(2),10(2) -- Test Request Message Send to Test Harness Server: \n " + msg.ToString();
                Dispatcher.Invoke(() => { OnNewMessageHandler(msgToDisplay); });
                sndr.routeRequests(msg);
                hrt.Stop();
                Console.WriteLine("# Requirement 12(1) -- Communication Latency for posting test request at Test Harness Server {0} ", hrt.ElapsedMicroseconds+"ms");
                Dispatcher.Invoke(() => { OnNewMessageHandler("# Requirement 12(1) -- Time Required for each test reqeuest process {0} " + hrt.ElapsedMicroseconds + "ms"); });
                ThreadProc();
                dispFilesMsg(filesBlockQue);
                dispTestResultsMsg(tstResultsBlockQ);
               
                TestMessages createMsg = new TestMessages();
                Message queryReq = createMsg.makeMessage(RemoteLogRequestBox.Text, LocalLogRequestBox.Text, "LogsQuery", AuthorName.Text, "ShowAllLogFiles");
                Dispatcher.Invoke(() => { OnNewMessageHandler("# Requirement 9(1) -- Query Request Message sent to the Repository: \n " + queryReq.ToString()); });
                sndr = intialize(RemoteLogRequestBox.Text);
                sndr.routeRequests(queryReq);              
            }
            catch (Exception ex)
            {       Console.WriteLine("Exception caused in Client GUI {0}", ex.Message);
            }          
        }

        //It is used to get all files stored corresponding to the author name in repository
        private void Query_Submit_Click(object sender, RoutedEventArgs e)
        { try
            {
                TestMessages createMsg = new TestMessages();
                Message queryReq = createMsg.makeMessage(RemoteLogRequestBox.Text, LocalLogRequestBox.Text, "LogsQuery", AuthorNameR.Text, "ShowAllLogFiles");
                Dispatcher.Invoke(() => { OnNewMessageHandler("# Requirement 9(1) -- Query Request Message sent to the Repository: \n " + queryReq.ToString()); });
                sndr = intialize(RemoteLogRequestBox.Text);
                sndr.routeRequests(queryReq);
                ThreadProc();
                dispTestResultsMsg(tstResultsBlockQ);
            }
            catch (Exception ex)
            {             
                Console.WriteLine("Exception caused in Query Reqeuest of all files of author {0}", ex.Message); }
        }
        
            //Used to add the logs to the list box where logs are displayed on GUI
        void OnNewMessageHandler(string msg)
        {
            listBox1.Items.Insert(0, msg);
            listBox1.Items.SortDescriptions.Add(
            new System.ComponentModel.SortDescription(msg,
            System.ComponentModel.ListSortDirection.Ascending));          
        }

        //used to unload the listener adn sender channels 
        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            TestMessages createMsg = new TestMessages();
            Message temp = createMsg.makeMessage(RemoteLogRequestBox.Text, LocalLogRequestBox.Text, "", AuthorName.Text, "quit");
            sndr.routeRequests(temp);
            sndr.Close();
            recvr.Close();
        }

        public MainWindow()
        {        
            InitializeComponent();
            string[] args = Environment.GetCommandLineArgs();
            if (isDefined == false)
            {
                recvr = new Receiver();
                if (args.Length!=1)
                {
                    recvr.CreateRecvChannel(args[1]);
                    Dispatcher.Invoke(() => LocalAddr1.Text = args[1]);
                }
                else
                {
                    recvr.CreateRecvChannel(LocalAddr1.Text);
                }            
            }
            isDefined = true;
                     
        }
    }
}
