////////////////////////////////////////////////////////////////////////
// HiResTimer.cs - To measure the latency and throughput              //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * This module provides operations to measure the latency and throughput.
 *  
/*
 * Build Process:
 * ==============
 * Files Required:
 *   HiResTimer.cs
 *   
 * public Interface:  
 *   int QueryPerformanceFrequency( out ulong x);
 *   int QueryPerformanceCounter( out ulong x);
 *   
 * Compiler Command:
 *   csc /t:exe HiResTimer.cs
 *   
 * Maintainence History:
 * ==================
 * ver 1.0 : 20 Nov 2016
 *   - first release
 * 
 */
//
using System;
using System.Runtime.InteropServices; // for DllImport attribute
using System.ComponentModel; // for Win32Exception class
using System.Threading; // for Thread.Sleep method

namespace HRTimer
{
   public class HiResTimer
   {
     protected ulong a, b, f;
     
     public HiResTimer()
      {
         a = b = 0UL;
         if ( QueryPerformanceFrequency( out f) == 0) 
            throw new Win32Exception();
      }

      public ulong ElapsedTicks
      {
         get
         { return (b-a); }
      }

      public ulong ElapsedMicroseconds
      {
         get
         { 
            ulong d = (b-a); 
            if (d < 0x10c6f7a0b5edUL) // 2^64 / 1e6
               return (d*1000000UL)/f; 
            else
               return (d/f)*1000000UL;
         }
      }

      public TimeSpan ElapsedTimeSpan
      {
         get
         { 
            ulong t = 10UL*ElapsedMicroseconds;
            if ((t&0x8000000000000000UL) == 0UL)
               return new TimeSpan((long)t);
            else
               return TimeSpan.MaxValue;
         }
      }

      public ulong Frequency
      {
         get
         { return f; }
      }

      public void Start()
      {
         Thread.Sleep(0);
         QueryPerformanceCounter( out a);
      }

      public ulong Stop()
      {
         QueryPerformanceCounter( out b);
         return ElapsedTicks;
      }

     // Here, C# makes calls into C language functions in Win32 API
     // through the magic of .Net Interop

      [ DllImport("kernel32.dll", SetLastError=true) ]
      protected static extern 
         int QueryPerformanceFrequency( out ulong x);

      [ DllImport("kernel32.dll") ]
      protected static extern 
         int QueryPerformanceCounter( out ulong x);

        static void Main()
        {
            HiResTimer hrt = new HiResTimer();
            hrt.Start();
            hrt.Stop();
            Console.WriteLine("Time start {0}",hrt.ElapsedMicroseconds);    
        }
   }
}