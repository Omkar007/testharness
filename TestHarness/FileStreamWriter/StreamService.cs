///////////////////////////////////////////////////////////////////////
// StreamService.cs - WCF StreamService in Self Hosted Configuration //
//                                                                   //
// Omkar Patil, CSE681 - Software Modeling and Analysis, Fall 2016   //
///////////////////////////////////////////////////////////////////////
/*
 * Note:
 * - Uses Programmatic configuration, no app.config file used.
 * - Uses ChannelFactory to create proxy programmatically. 
 * - Expects to find ToSend directory under application with files
 *   to send.
 * - Will create SavedFiles directory if it does not already exist.
 * - Users HRTimer.HiResTimer class to measure elapsed microseconds.
 */

using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;

namespace FileStreamWriter
{
  [ServiceBehavior(IncludeExceptionDetailInFaults=true)]
  public class StreamService : IStreamService
  {
    string filename;
    static string savePath = "Repository/dllFiles";
    static int BlockSize = 1024;
    static byte[] block;
    ServiceHost host;

//parameterized constructor used to initialize the Listener
   public static StreamService NewHost(string url)
    {
            try
            {
                StreamService service = new StreamService();
                block = new byte[BlockSize];
                service.host = service.CreateServiceChannel(url);
                service.host.Open();
                return service;
            }
            catch 
            { return null; }
          
    }
        //used to close listener connection
       ~StreamService()
        {
            try
            {
                if (host != null)
                {      host.Close();
                }
            }
            catch (Exception)
            {
            }        
        }

        //used to check file available at repository 
        public bool CheckFileAvailability(string filename)
        {
            string FilePath = Path.Combine(savePath, filename);

            if (!File.Exists(FilePath))
            {
                return false;
            }
            return true;
        }

        //used to write the files from the Filetransfer msg stream to the directory
        public void upLoadFile(FileTransferMessage msg)
        { try {
                int totalBytes = 0;
                HRTimer.HiResTimer hrt = new HRTimer.HiResTimer();
                hrt.Start();
                filename = msg.filename;
                string rfilename = Path.Combine(savePath, filename);
                if (!Directory.Exists(savePath))
                    Directory.CreateDirectory(savePath);
                using (var outputStream = new FileStream(rfilename, FileMode.Create))
                {
                    while (true)
                    {
                        int bytesRead = msg.transferStream.Read(block, 0, BlockSize);
                        totalBytes += bytesRead;
                        if (bytesRead > 0)
                            outputStream.Write(block, 0, bytesRead);
                        else
                            break;
                    } }
                hrt.Stop();
                Console.Write( "\n  Received file \"{0}\" of {1} bytes in {2} microsec.", filename, totalBytes, hrt.ElapsedMicroseconds);
            }
            catch (Exception) { }
            
    }

        //used to get the stream of dll from the path specified where dll files are placed
    public Stream downLoadFile(string filename)
        {
            HRTimer.HiResTimer hrt = new HRTimer.HiResTimer();
            hrt.Start();
      string sfilename = Path.Combine(savePath,filename);
      FileStream outStream = null;
            if (File.Exists(sfilename))
            {
                outStream = new FileStream(sfilename, FileMode.Open);
            }
            else {
                throw new Exception("open failed for \"" + filename + "\"");
            }
     
      hrt.Stop();
      Console.Write("\n  Sent \"{0}\" in {1} microsec.", filename, hrt.ElapsedMicroseconds);
      return outStream;
    }

        //used to create the service host to accept the incoming requests
    ServiceHost CreateServiceChannel(string url)
    {
      // Can't configure SecurityMode other than none with streaming.
      // This is the default for BasicHttpBinding.
      //   BasicHttpSecurityMode securityMode = BasicHttpSecurityMode.None;
      //   BasicHttpBinding binding = new BasicHttpBinding(securityMode);

      BasicHttpBinding binding = new BasicHttpBinding();
      binding.TransferMode = TransferMode.Streamed;
      binding.MaxReceivedMessageSize = 50000000;
      Uri baseAddress = new Uri(url);
      Type service = typeof(FileStreamWriter.StreamService);
      ServiceHost host = new ServiceHost(service, baseAddress);
      host.AddServiceEndpoint(typeof(IStreamService), binding, baseAddress);
      return host;
    }
        //test stub
        public static void Main()
        {
            string FileHostingUrl = @"http://localhost:8000/StreamService";
            Thread t = new Thread(url =>
            {
                StreamService srvce;
                srvce = StreamService.NewHost((string)url);
            });
            t.Start(FileHostingUrl);
            Console.Write("\n  SelfHosted File Stream Service started");
            Console.Write("\n ========================================\n");
            Console.Write("\n  Press key to terminate service:\n");
            Console.ReadKey();
            Console.Write("\n");
         
        }
    }
}
