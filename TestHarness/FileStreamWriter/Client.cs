﻿///////////////////////////////////////////////////////////////////////
// Client.cs - WCF Timed, SelfHosted, File StreamService client      //
//                                                                   //
// Omkar Patil, CSE681 - Software Modeling and Analysis, Fall 2016   //
///////////////////////////////////////////////////////////////////////
/*
 * Note:
 * - Uses Programmatic configuration, no app.config file used.
 * - Uses ChannelFactory to create proxy programmatically. 
 * - Expects to find ToSend directory under application with files
 *   to send.
 * - Will create SavedFiles directory if it does not already exist.
 */

using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace FileStreamWriter
{
  public class Client
  {
    IStreamService channel;
    int BlockSize = 1024;
    byte[] block;
    HRTimer.HiResTimer hrt = null;

    //constructor used to initialize the Client side streamer
     public Client(string url)
    {
      channel = null;
      block = new byte[BlockSize];
      hrt = new HRTimer.HiResTimer();
      CreateServiceChannel(url);

     }
        //Destructor used to destroy the host 
        ~Client()
        {
            if (channel!=null)
            {
                ((IChannel)channel).Close();
            }
        }


        //used for creating Service channel 
    public void CreateServiceChannel(string url)
    {
      BasicHttpSecurityMode securityMode = BasicHttpSecurityMode.None;
      BasicHttpBinding binding = new BasicHttpBinding(securityMode);
      binding.TransferMode = TransferMode.Streamed;
      binding.MaxReceivedMessageSize = 500000000;
      EndpointAddress address = new EndpointAddress(url);
      ChannelFactory<IStreamService> factory= new ChannelFactory<IStreamService>(binding, address);
      channel=factory.CreateChannel();
    }
  
        //used to upload the files by calling the fileupload method and passing msg as filestream
   public void uploadFile(string ToSendPath,string filename)
    {
      string fqname = Path.Combine(ToSendPath, filename);
      try
      {
        hrt.Start();
        using (var inputStream = new FileStream(fqname, FileMode.Open))
        {
          FileTransferMessage msg = new FileTransferMessage();
          msg.filename = filename;
          msg.transferStream = inputStream;
          channel.upLoadFile(msg);
        }
        hrt.Stop();
        Console.Write("\n  Uploaded file \"{0}\" in {1} microsec.", filename, hrt.ElapsedMicroseconds);
      }
      catch(Exception)
      {
      }
    }

   //used to download the files by caling download method and save them in folder created
   public void download(string SavePath,string filename)
    {
      int totalBytes = 0;
      try
      { hrt.Start();
        Stream strm = channel.downLoadFile(filename);
        string rfilename = Path.Combine(SavePath, filename);
        if (!Directory.Exists(SavePath))
          Directory.CreateDirectory(SavePath);
        using (var outputStream = new FileStream(rfilename, FileMode.Create))
        {
          while (true)
          {
            int bytesRead = strm.Read(block, 0, BlockSize);
            totalBytes += bytesRead;
            if (bytesRead > 0)
              outputStream.Write(block, 0, bytesRead);
            else
              break;
          }
        }
        hrt.Stop();
        ulong time = hrt.ElapsedMicroseconds;
        Console.Write("\n  Received file \"{0}\" of {1} bytes in {2} microsec.", filename, totalBytes, time);
      }
      catch (Exception){}
    }
        //used to check file available at repository 
        public bool CheckFileAvailability(string Filename)
        {
            return channel.CheckFileAvailability(Filename);
        }

        //test stub
        static void Main()
        {
            Console.Write("\n  Client of SelfHosted File Stream Service");
            Console.Write("\n ==========================================\n");
            Client clnt = new Client("http://localhost:8000/StreamService");
            HRTimer.HiResTimer hrt = new HRTimer.HiResTimer();
            StreamService svc = new StreamService();
            string ToSendPath = "..\\..\\..\\TestHarness\\dllRepository";
            string SavePath = "..\\..\\..\\TestHarness\\dllRepository\\Omkar Patil";
            hrt.Start();
            clnt.uploadFile(ToSendPath, "TestDriver1.dll");
            clnt.uploadFile(ToSendPath, "TestDriver2.dll");
            clnt.uploadFile(ToSendPath, "TestCode1.dll");
            clnt.uploadFile(ToSendPath, "TestCode2.dll");
            hrt.Stop();
            Console.Write("\n\n  total elapsed time for uploading = {0} microsec.\n", hrt.ElapsedMicroseconds);
            hrt.Start();
            clnt.download(SavePath, "TestDriver1.dll");
            clnt.download(SavePath, "TestDriver2.dll");
            clnt.download(SavePath, "TestCode1.dll");
            clnt.download(SavePath, "TestCode2.dll");
            hrt.Stop();
            Console.Write("\n\n  Total elapsed time for downloading = {0}",hrt.ElapsedMicroseconds);
            Console.Write("\n\n  Press key to terminate client");
            Console.ReadKey();
            Console.Write("\n\n");
            ((IChannel)clnt.channel).Close();
        }
    }
}
