﻿////////////////////////////////////////////////////////////////////////
// SemiExpQueue.cs - Enqueue and Dequeue Test Requests                //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * This module provides operations to Enqueue and Dequeue Test Requests
 * given by the user.
 *  
/*
 * Build Process:
 * ==============
 * Files Required:
 *   SemiExpQueue.cs
 *   
 * public Interface:  
 *   void processRequest(string request);
 *   int noOfRequests();
 *   string dequeueRequests();
 *   
 * Compiler Command:
 *   csc /t:exe SemiExpQueue.cs
 *   
 * Maintainence History:
 * ==================
 * ver 1.0 : 05 Oct 2016
 *   - first release
 * 
 */
//
using System;
using System.Collections;
using System.Threading;

namespace BlockQueue
{
    public class BlockingQueue<T>
    {
        object locker_ = new object();
        private Queue blockingQ;

        //----< constructor >--------------------------------------------

        public BlockingQueue()
        {
            blockingQ = new Queue();
        }

        //This function is used to enqueue the user requests
        public void enqueueRequest(T request)
        {
        
            lock (locker_)  // uses Monitor
            {
                blockingQ.Enqueue(request);
                Monitor.Pulse(locker_);
            }
        }

        //This function is used to Dequeue the user requests
        public T dequeueRequests()
        {
            T testRequest = default(T);
            lock (locker_)
            {
                while (this.noOfRequests() == 0)
                {
                    Monitor.Wait(locker_);
                }
                testRequest = (T)blockingQ.Dequeue();

            }
            return testRequest;
        }

        //This function is used to get the count of the testRequests in the Queue
        public int noOfRequests()
        {
            int countXmlRequests = 0;
            lock (locker_)
            {
                countXmlRequests = blockingQ.Count;
            }

            return countXmlRequests;
        }
    }
    //test stub
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("\n  Testing Monitor-Based Blocking Queue");
            Console.Write("\n ======================================");

            BlockQueue.BlockingQueue<string> q = new BlockingQueue<string>();
            Thread t = new Thread(() =>
            {
                string testRequest;
                while (true)
                {
                    testRequest = q.dequeueRequests(); Console.Write("\n  child thread received {0}", testRequest);
                    if (testRequest == "quit") break;
                }
            });
            t.Start();
            string sendMsg = "testRequest #";
            for (int i = 0; i < 20; ++i)
            {
                string temp = sendMsg + i.ToString();
                Console.Write("\n test requests send by main thread {0}", temp);
                q.enqueueRequest(temp);
            }
            q.enqueueRequest("quit");
            t.Join();
            Console.Write("\n\n");
            Console.ReadLine();

        }
    }
}
