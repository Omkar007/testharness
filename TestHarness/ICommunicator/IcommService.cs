﻿////////////////////////////////////////////////////////////////////////
// IcommService.cs - declares IcommService interface                  //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * This module provides the IcommService interface, declaring sendRequests(Message msg),Message getResults().
 *
 * Public Interface:
 * =================
 *   void sendRequests(Message msg);
 *   Message getResults();
 */
/*
 * Build Process:
 * ==============
 * Files Required:
 *   IcommService.cs
 * Compiler Command:
 *   csc /t:Library IcommService.cs
 *  
 * Maintence History:
 * ==================
 * ver 1.0 : Nov 20 2016
 *   - first release
 * 
 */
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Messages;

namespace FileStreamWriter
{
    [ServiceContract]
    public interface IcommService
    {
        //used to send messages should be implmented by all class impleneting this interface
        [OperationContract(IsOneWay = true)]
        void sendRequests(Message msg);

        //used to get the Messages posted , dequeue is performed in this function
        Message getResults();
    }
}
