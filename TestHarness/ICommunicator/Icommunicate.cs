﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Messages;

namespace ICommunicator
{
    [ServiceContract]
    public interface Icommunicate
    {
        [OperationContract(IsOneWay = true)]
        void sendRequests(Message msg);

        [OperationContract]
        string getResults();
    }
}
