﻿///////////////////////////////////////////////////////////////////////////
// IStreamService.cs - WCF StreamService in Self Hosted Configuration    //
//                                                                       //
// Omkar Patil, CSE681 - Software Modeling and Analysis, Fall 2016       //
///////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.ServiceModel;

namespace FileStreamWriter
{
  [ServiceContract]
  public interface IStreamService
  {
    //used for uploading the file as stream
    [OperationContract(IsOneWay=true)]
    void upLoadFile(FileTransferMessage msg);

    //used for downloading the file as stream
    [OperationContract]
    Stream downLoadFile(string filename);

   [OperationContract]
   bool CheckFileAvailability(string filename);
  }

  [MessageContract]
  public class FileTransferMessage
  {
    [MessageHeader(MustUnderstand = true)]
    public string filename { get; set; }

    [MessageBodyMember(Order = 1)]
    public Stream transferStream { get; set; }
  }
}