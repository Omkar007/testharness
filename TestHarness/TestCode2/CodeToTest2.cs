﻿////////////////////////////////////////////////////////////////////////
// CodeToTest2.cs - Test Code to perform Addition of two numbers      //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * This module includes the Code to perform addition operation on two numbers
 * passed as parameter.
 *  
/*
 * Build Process:
 * ==============
 * Files Required:
 *   CodeToTest2.cs
 *   
 * Compiler Command:
 *   csc /t:exe CodeToTest2.cs
 *   
 * Maintainence History:
 * ==================
 * ver 1.0 : 05 Oct 2016
 *   - first release
 * 
 */
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCode2
{
    public class CodeToTest2
    {
        //This function perform Addition of two numbers
        public int addition(int a, int b)
        {
            int c = a+b;
            return c;
        }

        //test stub
        static void Main(string[] args)
        {
            try
            {

                CodeToTest2 sumRes = new CodeToTest2();
                int k = sumRes.addition(10, 2);
                Console.WriteLine("Result of addition is {0}", k);
                Console.ReadKey();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
