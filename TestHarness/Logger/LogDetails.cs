﻿////////////////////////////////////////////////////////////////////////
// LogDetails.cs - Store and Retrieve Test results of User            //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * This module provides operations to store the test results into the file 
 * and provide a mechanism to retrieve the logs.
 *  
/*
 * Build Process:
 * ==============
 * Files Required:
 *   LogDetails.cs
 *   
 * public Interface:
 * void storeRequestsLog(StringBuilder testResult, string authorName);
 * StringBuilder queryLogs(string repoStoragePath, string queryText);
 * StringBuilder displayFileLog(string repoStoragePath, string queryText);
 *   
 * Compiler Command:
 *   csc /t:exe LogDetails.cs
 *   
 * Maintainence History:
 * ==================
 * ver 1.1 : 20 Nov 2016
 *   - first release 1.0 : 05 Oct 2016
 */
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace Logger
{
    public interface Ilogger{

        string storeRequestsLog(string testResult,string repositoryPath, string authorName);
        StringBuilder queryLogs(string repoStoragePath, string queryText);
    }

    public class LogDetails:Ilogger
    {
        static object locker = new Object();

        //This function stores the test results into the file
        public string storeRequestsLog(string testResult, string repositoryPath, string authorName)
        {
            string logtext = null;
            string fileName = null;
          
            try
            {            
                string timeStamp = GetTimestamp(DateTime.Now);
                string path = System.IO.Path.GetFullPath(repositoryPath);
                string title = "Console Log: " + timeStamp;
                fileName = path + "\\" + authorName + "_" + timeStamp + ".txt";
                Console.WriteLine("File path is {0}",fileName);
                Console.WriteLine("\n ============= Requirement 8(1) ===============");
                Console.WriteLine("Storing Test execution results in {0}", authorName + "_" + timeStamp + ".txt");
                logtext = title + Environment.NewLine + testResult;
                lock (locker)
                {
                    using (StreamWriter outfile = new StreamWriter(fileName))
                    {
                        outfile.Write(logtext);
                    }
                }
            }
            catch (Exception e)
            {Console.WriteLine("Exception occured in storing the test results in repository {o}",e.Message);          
            }   
            return fileName;
        }

        // This function retrieves the log files of user's test request and displays them on console
        public StringBuilder queryLogs(string repoStoragePath, string queryText)
        {
            StringBuilder queryResults = new StringBuilder();
            string path = System.IO.Path.GetFullPath(repoStoragePath);
            string[] files = System.IO.Directory.GetFiles(repoStoragePath, "*.txt");
            foreach (string file in files)
            {
                if (file.Contains(queryText))
                {
                    string name = System.IO.Path.GetFileName(file);
                    queryResults.Append(name+"\n");
                }
            }

            return queryResults;
        }
        // This function retrieves the log of user's request file and displays them on console
        public StringBuilder displayFileLog(string repoStoragePath, string queryText)
        {
            StringBuilder queryResults = new StringBuilder();
            string path = System.IO.Path.GetFullPath(repoStoragePath);
            string[] files = System.IO.Directory.GetFiles(repoStoragePath, "*.txt");
            string getFile = repoStoragePath+"\\"+ queryText;
            foreach (string file in files)
            {        
                if (file.Equals(getFile))
                {
                    string contents = File.ReadAllText(file);
                    queryResults.Append(contents + "\n");
                }
            }
            return queryResults;
        }

        //used to get the timestamp
        public string GetTimestamp(DateTime value)
        {
            return value.ToString("yyyy_MM_dd-HH_mm_ss");
        }

        // test stub for testing logger methods
        static void Main(string[] args)
        {
            string text = "Testing TestDriver1" + Environment.NewLine +
                 "Test Passed " + Environment.NewLine;
       
            string authorName = "Omkar Patil";
            string repositoryPath = "../../../Repository/logFiles";
            StringBuilder queryResults = new StringBuilder();

            LogDetails log = new LogDetails();
            log.storeRequestsLog(text,repositoryPath, authorName);
            queryResults = log.queryLogs(repositoryPath,authorName);
            queryResults = log.displayFileLog(repositoryPath, "Omkar Patil_2016_11_18-02_11_18.txt");

            Console.ReadKey();
        }
    }  
}
