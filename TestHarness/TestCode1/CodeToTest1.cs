﻿////////////////////////////////////////////////////////////////////////
// CodeToTest1.cs - Test Code to perform Division of two numbers      //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * This module includes the Code to perform division operation on two numbers
 * passed as parameter.
 *  
/*
 * Build Process:
 * ==============
 * Files Required:
 *   CodeToTest1.cs
 *   
 * Compiler Command:
 *   csc /t:exe CodeToTest1.cs
 *   
 * Maintainence History:
 * ==================
 * ver 1.0 : 05 Oct 2016
 *   - first release
 * 
 */
//
using System;

namespace TestCode1
{
    public class CodeToTest1
    {
        //This function perform Division of two numbers
        public int divide(int a, int b)
        {
            int c = a / b;
            return c;
        }

        //test stub
        static void Main(string[] args)
        {
            try
            {

                CodeToTest1 divNum = new CodeToTest1();
                int k = divNum.divide(10, 2);
                Console.WriteLine("Result of Divisiom is {0}", k);
                Console.ReadKey();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}

