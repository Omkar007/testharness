﻿////////////////////////////////////////////////////////////////////////
// ITest.cs - declares ITest interface                                //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * This module provides the ITest interface, declaring bool test().
 *
 * Public Interface:
 * =================
 * bool test();
 */
/*
 * Build Process:
 * ==============
 * Files Required:
 *   ITest.cs
 * Compiler Command:
 *   csc /t:Library ITest.cs
 *  
 * Maintence History:
 * ==================
 * ver 1.0 : Oct 05 2016
 *   - first release
 * 
 */
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITest
{
    public interface Itest
    {
        bool test(); //to be implemented to test the test cases 
        string getLog(); //to get the logs written by the user
    }
    public interface ITestResult
    {
        string testName { get; set; }
        string testResult { get; set; }
        string testLog { get; set; }
    }
    public interface ITestResults
    {
        string testKey { get; set; }
        DateTime dateTime { get; set; }
        List<ITestResult> testResults { get; set; }
    }
}
