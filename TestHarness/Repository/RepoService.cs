﻿////////////////////////////////////////////////////////////////////////
// RepoService.cs - Sender and Receiver Endpoints processes           //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * This Module implements the Sender and Receiver at Receiver End. It uses 
 * blocking queue to send and retrieve messages and processes the request,
 * reply messages.
 * 
 *  
/*
 * Build Process:
 * ==============
 * Files Required:
 *   RepoService.cs
 *   
 * public Interface:  
 *   ServiceHost CreateChannel(string url);
 *   void sendRequests(Message testReqeust);
 *   Message getResults();
 *   void storeTestResults(BlockingQueue<Message> testResultsQ);
 *   void sendNotifications(string toUrl, string fromUrl, string type, string author, string body);
 *   void processOtherQueries(BlockingQueue<Message> otherReqsQ);
 *   void routeRequests(Message msg);
 *   
 * Compiler Command:
 *   csc /t:exe RepoService.cs
 *   
 * Maintainence History:
 * ==================
 * ver 1.0 : 20 Nov 2016
 *   - first release
 * 
 */
//
using System;
using System.Collections.Generic;
using System.ServiceModel;
using Messages;
using FileStreamWriter;
using BlockQueue;
using System.Threading;
using Logger;
using System.Text;

namespace TestHarness
{
    class RepositoryReceiver : IcommService
    {
        static BlockingQueue<Message> repRcvblockQue = null;
        static BlockingQueue<Message> testResultsQueu = null;
        static BlockingQueue<Message> otherRequests = null;
        static ServiceHost service;
        Thread rcvdThrd = null;

        //constructer dequeuing messages and enqueuing to othe queue for processing
        public RepositoryReceiver()
        {       
                //string url= "http://localhost:8085/RepoService";
                //service =CreateRecvChannel(url);            
                if (repRcvblockQue == null)
                { repRcvblockQue = new BlockingQueue<Message>();
                }
                if (testResultsQueu == null)
                {testResultsQueu = new BlockingQueue<Message>();
                }
                if (otherRequests == null)
                {otherRequests = new BlockingQueue<Message>();
                }
                rcvdThrd = new Thread(() => {
                    while (true)
                    {
                        Message msg = getResults();
                        if ((msg.type).Equals("testResultsQuery"))
                        {   testResultsQueu.enqueueRequest(msg);
                        }else
                        {
                            otherRequests.enqueueRequest(msg);
                        }}
                });
                rcvdThrd.IsBackground = true;
                rcvdThrd.Start();         
        }

        //used to close the listener connection
        ~RepositoryReceiver()
        {    
                if (service!=null)
                {    service.Close();
                }             
        }

        //  Create ServiceHost for Communication service
        public ServiceHost CreateRecvChannel(string address)
        {
            Console.WriteLine("# Requirement 10(2) -- Instantiating Listner at Repository Receiver End");
            WSHttpBinding binding = new WSHttpBinding();
            Uri baseAddress = new Uri(address);
            service = new ServiceHost(typeof(RepositoryReceiver), baseAddress);
            service.AddServiceEndpoint(typeof(IcommService), binding, baseAddress);
            service.Open();
            return service;
        }

        //used to send the messages
        public void sendRequests(Message testReqeust)
        {
            repRcvblockQue.enqueueRequest(testReqeust);
        }

        //used to get the messages by dequeuing the messages
        public Message getResults()
        {
            return repRcvblockQue.dequeueRequests();
        }

        //used to send store the test results messages to repository
        public static void storeTestResults(BlockingQueue<Message> testResultsQ)
        {
            string logsPath = "Repository\\logFiles";
            Thread tstResultThread = null;
            string requestType = null;
            string toUrl = null;
            string errMsg = null;
            string fromUrl = null;
            string filePath = null;
            try
            {  tstResultThread = new Thread(() =>
                {
                    while (true)
                    {
                        Message testResults = testResultsQ.dequeueRequests();
                        Console.WriteLine("# Requirement 7(2),10(2) -- Message from Test Harness To Repository to Store Logs: \n {0}",testResults.ToString());
                        LogDetails log = new LogDetails();
                        filePath=log.storeRequestsLog(testResults.body, logsPath,testResults.author);
                        requestType = "storeLogsReply"; toUrl = "http://localhost:8087/ClientService";
                        fromUrl = "http://localhost:8085/RepoService"; errMsg = "# Requirement 7(2),10(2) -- Your test execution results are stored in repository at path: " + filePath;
                        sendNotifications(toUrl,fromUrl, requestType, testResults.author, errMsg);
                    }
                });
                tstResultThread.IsBackground = true;
                tstResultThread.Start();
            }
            catch (Exception e)
            { Console.WriteLine("\nException occured at repository end while storing test Results Msgs from Queue {0}", e.Message); }
        }

        //used to send the notification messages to the Client
        public static void sendNotifications(string toUrl, string fromUrl, string type, string author, string body)
        {
            try
            {
                HRTimer.HiResTimer hrt = new HRTimer.HiResTimer();
                hrt.Start();
                RepositorySender client = initializeSender(toUrl);
                TestMessages msg = new TestMessages();
                Message thresponse = msg.makeMessage(toUrl, fromUrl, type, author, body);
                Console.WriteLine("# Requirement 10(2) -- Sending notification Messages to Client:\n {0}",thresponse.ToString());
                client.routeRequests(thresponse);
                hrt.Stop();               
                Console.WriteLine("\nTime Elapsed in sending notification {0} ms", hrt.ElapsedMicroseconds);
            }
            catch (Exception e)
            {
                Console.WriteLine("\nException occurred in sending notification message of test results stored to Client {0}", e.Message);
            }
        }

        //used to process other request from the queue and send messages to the respective server
        public static void processOtherQueries(BlockingQueue<Message> otherReqsQ)
        {
            string requestType = null;
            string toUrl = null;
            string fromUrl = null;
            Thread otherReqThread = null;
            StringBuilder queryResults = null;
            string repositoryPath = "Repository/logFiles";
            try
            { otherReqThread = new Thread(() =>
                { while (true)
                {  Message query = otherReqsQ.dequeueRequests();
                        if (query.type.Equals("LogsQuery"))
                        {
                            LogDetails log = new LogDetails();
                            if (query.body.Equals("ShowAllLogFiles"))
                            { queryResults = log.queryLogs(repositoryPath, query.author);
                                requestType = "LogsReply";}
                            else
                            { queryResults = log.displayFileLog(repositoryPath,query.body);
                                requestType = "LogFileReply"; }                   
                            toUrl = "http://localhost:8087/ClientService";
                            fromUrl = "http://localhost:8085/RepoService";
                            sendNotifications(toUrl, fromUrl, requestType, query.author, queryResults.ToString());
                        }}
            });
                otherReqThread.IsBackground = true;
                otherReqThread.Start();
            }
            catch(Exception e)
            {Console.WriteLine("\n Exception occured in processing other requests queue {0}",e.Message);}
        }

        //used to initialize repository sender
        public static RepositorySender initializeSender(string url)
        {
            try
            {   RepositorySender client = new RepositorySender(url);
                return client;
            }
            catch (Exception e)
            {
                Console.WriteLine("\nException occurred in Initializing Sender {0}", e.Message);
                return null;
            }
        }

        //test stub
        static void Main(string[] args)
        {
            Console.Title = "Repository";
            Console.Write("\n ================ Respository =====================\n");            
            LogDetails getLogs = new LogDetails();
            List<string> queryResults = new List<string>();
            string url = "http://localhost:8085/RepoService";
            try
            {
                RepositoryReceiver rcvs = new RepositoryReceiver();
                rcvs.CreateRecvChannel(url);
                if (testResultsQueu!=null) { storeTestResults(testResultsQueu); }
                if (otherRequests!=null) { processOtherQueries(otherRequests); }
               
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.Write("\n\n  {0}\n\n", ex.Message);
                return;
            }
        }
    }

    class RepositorySender
    {
        IcommService svcr;
        BlockingQueue<Message> sndBlockQRepository = null;
        Thread sndThrd = null;
        int tryCount = 0, MaxCount = 5;
        string lastError = "";

        //used for creating the proxy channel and sending the messages to the other servers
        void ThreadProc(string url)
        {
            try
            {  while (true)
                {  Message msg = sndBlockQRepository.dequeueRequests();
                    if (url.Equals(msg.toUrl)) { }
                    else
                    { while (true)
                        {    try
                            {  svcr = CreateProxy<IcommService>(msg.toUrl);
                                tryCount = 0;
                                break;
                            }
                            catch (Exception ex)
                            {
                                if (++tryCount < MaxCount)
                                    Thread.Sleep(100);
                                else
                                {  lastError = ex.Message;
                                    break;
                                }  }   }
                    }
                    svcr.sendRequests(msg);
                    if (msg.body == "quit")
                        break;
                }  }
            catch (Exception) { } 
        }

        //constructor used for creating channel and spwanning sender thread
       public RepositorySender(string url)
        {
            sndBlockQRepository = new BlockingQueue<Message>();
            while (true)
            {
                try
                { svcr = CreateProxy<IcommService>(url);
                    tryCount = 0;
                    break;
                }
                catch (Exception ex)
                {
                    if (++tryCount < MaxCount)
                        Thread.Sleep(100);
                    else
                    {
                        lastError = ex.Message;
                        break;
                    }
                }
            }
            sndThrd = new Thread(() => ThreadProc(url));
            sndThrd.IsBackground = true;
            sndThrd.Start();
        }

        //method to create channel of communicaiton 
        public C CreateProxy<C>(string url)
        {
            WSHttpBinding binding = new WSHttpBinding();
            EndpointAddress address = new EndpointAddress(url);
            ChannelFactory<C> factory = new ChannelFactory<C>(binding, address);
            return factory.CreateChannel();
        }

        //used to close the communication channel
        public void Close()
        {
            ChannelFactory<IcommService> temp = (ChannelFactory<IcommService>)svcr;
            temp.Close();
        }

        //used to enqueue messages in sender queueue by others
        public void routeRequests(Message msg)
        {
            sndBlockQRepository.enqueueRequest(msg);
        }
    }
}
