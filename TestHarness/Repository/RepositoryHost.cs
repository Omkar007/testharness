﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using ICommunicator;

namespace Repository
{
    class RepositoryHost
    {
        static ServiceHost CreateChannel(string url)
        {
            WSHttpBinding binding = new WSHttpBinding();
            Uri address = new Uri(url);
            Type service = typeof(RepoService);
            ServiceHost host = new ServiceHost(service, address);
            host.AddServiceEndpoint(typeof(IcommService), binding, address);
            return host;
        }
        static void Main(string[] args)
        {
            Console.Title = "WSHttp Repository side Server Hosted";
            Console.Write("\n =====================================\n");

            ServiceHost host = null;
            try
            {
                host = CreateChannel("http://localhost:8085/RepoService");
                host.Open();
                Console.Write("\n  Started WSHttpService - Press key to exit:\n");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.Write("\n\n  {0}\n\n", ex.Message);
                return;
            }
            host.Close();
        }
    }
}
