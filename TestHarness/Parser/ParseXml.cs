﻿////////////////////////////////////////////////////////////////////////
// ParseXml.cs - Parses Xml Request and returns list of Test Object   //
//                                                                    //
// Platform:    HP Pavilion, Windows 10                               //
// Application: CSE681 - Software Modelling Analysis, Test Harness    //
// Author:      Omkar Patil, Syracuse University,                     //
//              ospatil@syr.edu, (315) 949-8810                       //
////////////////////////////////////////////////////////////////////////
/*
 * Module Operations:
 * ==================
 * This module provides operations to parse the Xml test Request and return
 * the List of the Test Object
 *  
/*
 * Build Process:
 * ==============
 * Files Required:
 *   ParseXml.cs
 *   
 * public Interface:
 * List<Test> parse(System.IO.Stream ios);
 *   
 * Compiler Command:
 *   csc /t:exe ParseXml.cs
 *   
 * Maintainence History:
 * ==================
 * ver 1.0 : 05 Oct 2016
 *   - first release
 * 
 */
//
using ITest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Parser
{
    [Serializable]
    public class Test
    {
        public String testName { get; set; }
        public String author { get; set; }
        public DateTime timestamp { get; set; }
        public String testDriver { get; set; }
        public List<String> testCode { get; set; }

        // Displays parsed xml for test stub
        public void show()
        {
            Console.Write("\n  {0,-12} : {1}", "test name", testName);
            Console.Write("\n  {0,12} : {1}", "author", author);
            Console.Write("\n  {0,12} : {1}", "time stamp", timestamp);
            Console.Write("\n  {0,12} : {1}", "test driver", testDriver);
            foreach (string library in testCode)
            {
                Console.Write("\n  {0,12} : {1}", "library", library);
            }

        }
    }

    [Serializable]
    public class TestData
    {
       public string Name { get; set; }
       public Itest testDriver { get; set; }
    }

    public class ParseXml
    {
        private XDocument doc;
        private List<Test> testList;

        public ParseXml()
        {
            doc = new XDocument();
            testList = new List<Test>();
        }

        //This function parses xml file and store the contents in object of type Test in List
        public List<Test> parse(System.IO.Stream ios)
        {
            doc = XDocument.Load(ios);
            if (doc == null)
            {
                return testList;
            }
            else
            {
                String author = doc.Descendants("author").First().Value;
                Test test = null;

                XElement[] xtests = doc.Descendants("test").ToArray();

                for (int i = 0; i < xtests.Count(); i++)
                {
                    test = new Test();
                    test.testCode = new List<string>();
                    test.author = author;
                    test.timestamp = DateTime.Now;
                    test.testName = xtests[i].Attribute("name").Value;
                    test.testDriver = xtests[i].Element("testDriver").Value;
                    IEnumerable<XElement> xtestCode = xtests[i].Elements("library");
                    foreach (var xlibrary in xtestCode)
                    {
                        test.testCode.Add(xlibrary.Value);
                    }
                    testList.Add(test);

                }

                return testList;
            }

        }

            //test stub
            static void Main(string[] args)
              {
            ParseXml checkParsing = new ParseXml();

                  try
                  {
                      string path = "../../testRequest.xml";
                      System.IO.FileStream xml = new System.IO.FileStream(path, System.IO.FileMode.Open);
                      List<Test> testResults=checkParsing.parse(xml);
                      if (testResults.Capacity>0)
                      {
                          foreach (Test test in testResults)
                          {
                              string testDriver = test.testDriver;
                              Console.WriteLine(testDriver);

                              List<string> testCodeList = test.testCode;
                              foreach(string testCode in testCodeList)
                              {
                                  string ts = testCode;
                                  Console.WriteLine(ts);
                              }
                          }

                      }
                      Console.ReadKey();
                  }
                  catch (Exception e)
                  {
                      Console.Write("Exception {0}", e.Message);
                      Console.ReadKey();
                  }
              } 

    }

}
